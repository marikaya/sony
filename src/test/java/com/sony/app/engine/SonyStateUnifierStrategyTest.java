package com.sony.app.engine;

import com.sony.app.TestInitializor;
import com.sony.app.exception.WrongInputException;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.Assert.*;

public class SonyStateUnifierStrategyTest extends StateUnifierStrategyTest {

    Logger logger = Logger.getLogger(SonyStateUnifierStrategyTest.class);

    @Test
    public void testZ() {
        try {
            results = sonyStateUnifierStrtategy.unify();

            Assert.assertEquals(new ArrayList<>(Arrays.asList(2,4,5,6)), results);

        } catch (WrongInputException e) {
            logger.error(e.getMessage(),e);
            fail();
        }
    }
}