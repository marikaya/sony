package com.sony.app.engine;

import com.sony.app.TestInitializor;
import com.sony.app.conf.PropertyLoader;
import com.sony.app.exception.WrongInputException;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class StateUnifierStrategyTest extends TestInitializor{

    Logger logger = LogManager.getLogger(StateUnifierStrategy.class);
    StateUnifierStrategy sonyStateUnifierStrtategy;
    List results;

    @Before
    public void setUp(){
        this.sonyStateUnifierStrtategy = StateUnifierStrategy.getStrategy("sony", getInputDescriptor());

        Assert.assertEquals(sonyStateUnifierStrtategy.getClass(), SonyStateUnifierStrategy.class);
    }

    @Test
    public void testBunify() {
        try {
           results = this.sonyStateUnifierStrtategy.unify();
        } catch (WrongInputException e) {
            logger.error(e.getMessage(),e);
            fail();
        }
    }

    @Test
    public void testEwriteToFile() {

        List<Integer> results = Arrays.asList(1,2,3,5);
        boolean writeResult = sonyStateUnifierStrtategy.writeToFile(results, PropertyLoader.getInstance().getOutputFile());

        Assert.assertEquals(writeResult, true);


    }
}