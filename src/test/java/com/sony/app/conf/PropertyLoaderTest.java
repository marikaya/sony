package com.sony.app.conf;

import com.sony.app.TestInitializor;
import com.sony.app.exception.PropertyNotFoundException;
import junit.textui.TestRunner;
import org.apache.log4j.Logger;
import org.junit.*;
import org.junit.runners.MethodSorters;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.fail;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class PropertyLoaderTest extends TestInitializor{
    private static final Logger logger = Logger.getLogger(PropertyLoaderTest.class);

    @Test
    public void testEgetBaseFolder() {
        String baseFolder = getPropertyLoader().getBaseFolder();
        Assert.assertEquals(baseFolder, null);
    }

    @Test
    public void testFgetStrategy() {
        String strategy = getPropertyLoader().getStrategy();
        Assert.assertEquals(strategy, "sony");
    }

    @Test
    public void testGgetOutputFile() {
        String outputFile = getPropertyLoader().getOutputFile();
        Assert.assertEquals(outputFile, "output.txt");
    }
}