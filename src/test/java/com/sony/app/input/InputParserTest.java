package com.sony.app.input;

import com.sony.app.TestInitializor;
import com.sony.app.conf.PropertyLoader;
import com.sony.app.exception.PropertyNotFoundException;
import com.sony.app.exception.WrongInputException;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

public class InputParserTest extends TestInitializor{

   private static final Logger logger = LogManager.getLogger(InputParserTest.class);

    @Test
    public void parse() {
        try {
            InputDescriptor parse = getInputParser().parse();

            Assert.assertNotNull(parse);
            Assert.assertEquals(parse.getTestCaseCount(), 4);
            Assert.assertEquals(parse.getInputCases().size(), 4);
            Assert.assertEquals(parse.getInputCases().size(), parse.getTestCaseCount());
        } catch (IOException | WrongInputException e) {
            logger.error(e.getMessage(), e);
        }
    }
}