package com.sony.app;

import com.sony.app.conf.PropertyLoader;
import com.sony.app.conf.PropertyLoaderTest;
import com.sony.app.exception.PropertyNotFoundException;
import com.sony.app.exception.WrongInputException;
import com.sony.app.input.InputDescriptor;
import com.sony.app.input.InputParser;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.fail;

public class TestInitializor {
    private static final Logger logger = Logger.getLogger(TestInitializor.class);

    InputParser inputParser;
    InputDescriptor inputDescriptor;
    PropertyLoader propertyLoader       = PropertyLoader.getInstance();

    @Before
    public void init() {
        try {
            PropertyLoader.getInstance().initialize(null, "default.properties");

            inputParser = new InputParser(PropertyLoader.getInstance().getBaseFolder(), PropertyLoader.getInstance().getInputFileName());
            Assert.assertNotNull(inputParser);

            inputDescriptor = inputParser.parse();
            Assert.assertNotNull(inputDescriptor);


        } catch (PropertyNotFoundException | WrongInputException | IOException e) {
            logger.error(e.getMessage(), e);
            fail();
        }
    }

    @Test
    public void testAFail1() {
        try {
            propertyLoader.initialize("", "");
            fail();
        } catch (PropertyNotFoundException e) {
            Assert.assertEquals("Could not find properties", e.getMessage());
        }
    }

    @Test
    public void testBFail3() {
        try {
            propertyLoader.initialize(null, "default1.properties");
            fail();
        } catch (PropertyNotFoundException e) {
            Assert.assertEquals("Could not find properties", e.getMessage());
        }catch (NullPointerException e){
            assertNotNull(e);
        }
    }

    @Test
    public void testCinitialize() {
        try {
            propertyLoader.initialize(null, "default.properties");
        } catch (PropertyNotFoundException e) {
            fail();
        }
    }

    @Test
    public void testDgetInputFileName() {
        String inputFileName = propertyLoader.getInputFileName();

        Assert.assertEquals(inputFileName, "input.txt");
    }


    public InputParser getInputParser() {
        return inputParser;
    }

    public void setInputParser(InputParser inputParser) {
        this.inputParser = inputParser;
    }

    public InputDescriptor getInputDescriptor() {
        return inputDescriptor;
    }

    public void setInputDescriptor(InputDescriptor inputDescriptor) {
        this.inputDescriptor = inputDescriptor;
    }

    public PropertyLoader getPropertyLoader() {
        return propertyLoader;
    }

    public void setPropertyLoader(PropertyLoader propertyLoader) {
        this.propertyLoader = propertyLoader;
    }
}
