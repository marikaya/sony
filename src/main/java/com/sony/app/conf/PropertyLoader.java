package com.sony.app.conf;


import com.sony.app.exception.PropertyNotFoundException;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;


public class PropertyLoader {

  private final static Logger logger = LogManager.getLogger(PropertyLoader.class);

  private static final String DEFAULT_PROPERTIES_FILE = "default.properties";
  private static final String PROP_OUTPUT_FILENAME = "output.fileName";
  private static final String PROP_INPUT_BASEFOLDER = "input.baseFolder";
  private static final String PROP_INPUT_FILENAME = "input.fileName";
  private static final String PROP_STRATEGY = "strategy";

  private static volatile PropertyLoader instance;
  private Properties properties;

  private PropertyLoader() {
    properties = new Properties();
  }

  public static synchronized PropertyLoader getInstance() {
    if (PropertyLoader.instance == null) {
      PropertyLoader.instance = new PropertyLoader();
    }
    return PropertyLoader.instance;
  }

  /**
   * Initialize Input File According To Given Path
   *
   * @param confPath Base Path Of Input/Configuration Files
   */
  public void initialize(String confPath, String defaultPropertiesFile)
      throws PropertyNotFoundException {


    if (defaultPropertiesFile == null) {
      defaultPropertiesFile = DEFAULT_PROPERTIES_FILE;
    }

    InputStream input = null;
    try {

      if (confPath == null) {
        input = getClass().getClassLoader().getResourceAsStream(defaultPropertiesFile);
      } else {
        input = new FileInputStream(confPath + File.separator + defaultPropertiesFile);
      }

      // load a properties file
      this.properties.load(input);

    } catch (IOException ex) {
      throw new PropertyNotFoundException("Could not find properties");
    } finally {
      if (input != null) {
        try {
          input.close();
        } catch (IOException e) {
          logger.error("Exception -> " + e.getMessage(), e);
        }
      }
    }
  }

  /**
   * Gives Input File Name
   * 
   * @return input file name
   */
  public String getInputFileName() {
    return PropertyLoader.getInstance().getProperties().getProperty(PROP_INPUT_FILENAME);
  }

  /**
   * Gives Base Input Folder
   * 
   * @return Base Input Folder
   */
  public String getBaseFolder() {
    return PropertyLoader.getInstance().getProperties().getProperty(PROP_INPUT_BASEFOLDER);

  }

  /**
   * Gives Step Calculation Strategy
   * 
   * @return Calculation Strategy Name
   */
  public String getStrategy() {
    return PropertyLoader.getInstance().getProperties().getProperty(PROP_STRATEGY);

  }

  /**
   * Gives Output File To Write Output Of Calculation Results
   * 
   * @return Output File Name
   */
  public String getOutputFile() {
    return PropertyLoader.getInstance().getProperties().getProperty(PROP_OUTPUT_FILENAME);
  }

  public Properties getProperties() {
    return properties;
  }

  public void setProperties(Properties properties) {
    this.properties = properties;
  }



}
