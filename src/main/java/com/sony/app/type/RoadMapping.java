package com.sony.app.type;


import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class RoadMapping {
  private Integer city;
  private List<Integer> connectedCities = new ArrayList<>();

  public RoadMapping(Integer city) {
    this.city = city;
  }

  public Integer getCity() {
    return city;
  }

  public void setCity(Integer city) {
    this.city = city;
  }

  public List<Integer> getConnectedCities() {
    return connectedCities;
  }

  public void setConnectedCities(List<Integer> connectedCities) {
    this.connectedCities = connectedCities;
  }

  @Override
  public String toString() {
    return "RoadMapping{" + "city=" + city + ", connectedCities=" + connectedCities + '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o)
      return true;
    if (o == null || getClass() != o.getClass())
      return false;
    RoadMapping that = (RoadMapping) o;
    return Objects.equals(city, that.city) && Objects.equals(connectedCities, that.connectedCities);
  }

  @Override
  public int hashCode() {

    return Objects.hash(city, connectedCities);
  }
}
