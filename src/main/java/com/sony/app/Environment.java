package com.sony.app;


import com.sony.app.conf.PropertyLoader;
import com.sony.app.engine.StateUnifierStrategy;
import com.sony.app.exception.WrongInputException;
import com.sony.app.input.InputDescriptor;
import com.sony.app.input.InputParser;
import org.apache.log4j.Logger;
import java.io.IOException;
import java.util.List;

public class Environment {

  final static Logger logger = Logger.getLogger(Environment.class);

  private ApplicationInitializer applicationInitializer;

  private StateUnifierStrategy stateUnifierStrategy;

  public Environment() {
    if (logger.isTraceEnabled()) {
      logger.trace("Environment has been created");
    }
  }

  public static void main(String[] args) {
    Environment environment = new Environment();
    environment.calculate();
  }

  /**
   * Initialize Environment
   * 
   * @return Input Descriptor
   */
  public InputDescriptor initialize() {
    String propertyFolder = System.getProperty("countryunifier.property.folder");
    String environment = System.getProperty("countryunifier.environment");
    applicationInitializer = new ApplicationInitializer(propertyFolder, environment);

    boolean configurationLoaded = applicationInitializer.loadConfiguration();

    if (configurationLoaded) {

      if (logger.isInfoEnabled()) {
        logger.info("Environment Properties have been initialized successfully");
      }


      String baseFolder = PropertyLoader.getInstance().getBaseFolder();
      String inputFileName = PropertyLoader.getInstance().getInputFileName();


      if (logger.isInfoEnabled()) {
        logger.info("Input Base Folder -> " + baseFolder + ", Input File Name -> " + inputFileName);
      }

      try {

        InputParser inputParser = new InputParser(baseFolder, inputFileName);
        return inputParser.parse();

      } catch (WrongInputException e) {
        logger.error("Wrong Input -> " + e.getMessage(), e);
      } catch (IOException e) {
        logger.error("IO Exception " + e.getMessage(), e);
      }
    } else {
      if (logger.isInfoEnabled()) {
        logger.info("Configuration file could not find");
      }
    }
    return null;

  }

  /**
   * Calculate Results
   */
  public void calculate() {
    InputDescriptor inputDescriptor = this.initialize();

    logger.trace(inputDescriptor);
    if (inputDescriptor != null) {
      StateUnifierStrategy sony = StateUnifierStrategy.getStrategy("sony", inputDescriptor);

      List calculatedResults = null;
      try {
        calculatedResults = sony.unify();
        sony.writeToFile(calculatedResults, PropertyLoader.getInstance().getOutputFile());

      } catch (WrongInputException e) {
        logger.error("Expcetion -> " + e.getMessage(), e);
      }

      if (logger.isInfoEnabled()) {
        logger.info(calculatedResults);
      }
    } else {
      logger.error("Couldnt initialize the environment, please check configuration files");
    }
  }

  public StateUnifierStrategy getStateUnifierStrategy() {
    return stateUnifierStrategy;
  }

  public void setStateUnifierStrategy(StateUnifierStrategy stateUnifierStrategy) {
    this.stateUnifierStrategy = stateUnifierStrategy;
  }
}
