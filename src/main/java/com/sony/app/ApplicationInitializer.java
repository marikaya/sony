package com.sony.app;

import com.sony.app.conf.PropertyLoader;
import com.sony.app.exception.PropertyNotFoundException;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;


public class ApplicationInitializer {
  private static final Logger logger = LogManager.getLogger(ApplicationInitializer.class);
  private String propertyFolder;
  private String environment;


  public ApplicationInitializer(String propertyFolder, String environment) {
    this.propertyFolder = propertyFolder;
    this.environment = environment;
  }

  public boolean loadConfiguration() {
    StringBuilder environmentFile = new StringBuilder("default");
    if (this.environment != null) {
      environmentFile.append("-");
      environmentFile.append(this.environment);

    } else {
      if (logger.isInfoEnabled()) {
        logger.info("Envrionment will be initialize with default values");
      }
    }
    try {
      PropertyLoader.getInstance().initialize(this.propertyFolder, environment);
      if (logger.isInfoEnabled()) {
        logger.info("Environment initialized with property folder -> " + propertyFolder
            + ", Enviroment -> " + environment);
      }

      return true;
    } catch (PropertyNotFoundException e) {
      logger.error("Property File Not Found -> " + e.getMessage(), e);
    }

    return false;
  }
}
