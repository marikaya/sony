package com.sony.app.engine;

import com.sony.app.exception.WrongInputException;
import com.sony.app.input.InputDescriptor;
import org.apache.log4j.Logger;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public abstract class StateUnifierStrategy<R> {
  private static final Logger logger = Logger.getLogger(StateUnifierStrategy.class);

  /**
   * Find Out The Strategy To Calculate And Write Results
   * 
   * @param strategy Strategy Name
   * @param inputDescriptor Input Descriptor
   * @return
   */
  public static StateUnifierStrategy getStrategy(String strategy, InputDescriptor inputDescriptor) {
    if (strategy.equals("sony")) {
      return new SonyStateUnifierStrategy(inputDescriptor);
    }
    return null;
  }

  /**
   * Start Unifying Process
   * 
   * @return Gives Unifying Results Of Each Input Case
   * @throws WrongInputException
   */
  public abstract List<R> unify() throws WrongInputException;

  /**
   * Writes To Results To Given File
   * 
   * @param result Calculated Results From {@link StateUnifierStrategy#unify()}
   * @param fileName File Name To Write Results
   * @return Process Result
   */
  public boolean writeToFile(List<R> result, String fileName) {
    File file = new File(fileName);
    boolean exists = file.exists();
    if (!exists) {
      try {
        boolean newFile = file.createNewFile();
        if (!newFile) {
          logger.error("File could not create");
          return false;
        }
      } catch (Exception e) {
        logger.error("IO Exception -> " + e.getMessage(), e);
        return false;
      }
    }

    FileWriter fileWriter = null;
    try {
      fileWriter = new FileWriter(file);

      for (R r : result) {
        fileWriter.append(String.valueOf(r)).append("\r\n");
      }

      return true;
    } catch (IOException e) {
      logger.error("IO Exception -> " + e.getMessage(), e);
      return false;
    } finally {
      if (fileWriter != null) {
        try {
          fileWriter.close();
        } catch (IOException e) {
          logger.error("IO Exception -> " + e.getMessage(), e);
        }
      }
    }


  }
}
