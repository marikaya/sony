package com.sony.app.engine;

import com.sony.app.exception.WrongInputException;
import com.sony.app.input.InputCase;
import com.sony.app.input.InputDescriptor;
import com.sony.app.type.RoadMapping;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


public class SonyStateUnifierStrategy extends StateUnifierStrategy<Integer> {
  private static final Logger logger = LogManager.getLogger(SonyStateUnifierStrategy.class);
  private InputDescriptor inputDescriptor;

  /**
   * Sony Step Calculation Strategy
   *
   * @param inputDescriptor Mapped Input Model That Loaded From Configuration
   *        {@link InputDescriptor}
   */
  public SonyStateUnifierStrategy(InputDescriptor inputDescriptor) {
    this.inputDescriptor = inputDescriptor;
  }

  private List<Integer> calculate() throws WrongInputException {

    List<Integer> results = new ArrayList<>();
    int testCaseCount = inputDescriptor.getTestCaseCount();
    if (testCaseCount == inputDescriptor.getInputCases().size()) {
      for (InputCase inputCase : inputDescriptor.getInputCases()) {

        double step = mergeStates(inputCase.getRoadMappingMapping());
        double realStep = Math.ceil(step / 2);
        results.add((int) realStep);

        if (logger.isInfoEnabled()) {
          logger.info("Steps are calculated as " + (int) realStep + " for step at index "
              + inputDescriptor.getInputCases().indexOf(inputCase));
        }
      }
    } else {
      throw new WrongInputException("Different Test Case Count");
    }

    return results;
  }

  /**
   * Merge 2 Cities According To Rules Of Sony
   * 
   * @param roadMappingMapping Current Road Mapping Of All States
   * @return consumed step count while merging 2 state
   */
  private int mergeStates(Map<Integer, RoadMapping> roadMappingMapping) {

    List<RoadMapping> sorted = roadMappingMapping.values().stream()
        .sorted((o1, o2) -> o2.getConnectedCities().size() - o1.getConnectedCities().size())
        .collect(Collectors.toList());
    int step = 0;

    if (sorted.size() > 0) {
      RoadMapping biggest = sorted.get(0);
      step++;

      if (biggest.getConnectedCities().size() > 0) {
        Integer candidateCity = biggest.getConnectedCities().get(0);

        RoadMapping connectCityRoadMap = roadMappingMapping.get(candidateCity);

        if (connectCityRoadMap != null) {
          List<Integer> connectedCities = connectCityRoadMap.getConnectedCities();

          biggest.getConnectedCities().remove(candidateCity);
          connectedCities.remove(biggest.getCity());
          biggest.getConnectedCities().addAll(connectedCities);

          for (RoadMapping roadMapping : roadMappingMapping.values()) {
            boolean isRemoved = roadMapping.getConnectedCities().remove(candidateCity);
            if (isRemoved) {
              roadMapping.getConnectedCities().add(biggest.getCity());
            }
          }

          roadMappingMapping.remove(candidateCity);

          step += mergeStates(roadMappingMapping);
        }
      }
    }
    return step;
  }


  @Override
  public List<Integer> unify() throws WrongInputException {
    return this.calculate();
  }
}
