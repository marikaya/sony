package com.sony.app.exception;

public class PropertyNotFoundException extends Exception {

  public PropertyNotFoundException(String message) {
    super(message);
  }
}
