package com.sony.app.exception;

public class WrongInputException extends Exception {

  public WrongInputException(String message) {
    super(message);
  }
}
