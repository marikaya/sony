package com.sony.app.input;

import com.sony.app.exception.WrongInputException;
import com.sony.app.type.RoadMapping;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class InputParser {

  private static Logger logger = LogManager.getLogger(InputParser.class);
  private String inputBaseFolder;
  private String inputFileName;

  /**
   * Input Parse
   * 
   * @param inputBaseFolder Base Folder To Search Input File
   * @param inputFileName Input Fila Name
   */
  public InputParser(String inputBaseFolder, String inputFileName) {

    this.inputBaseFolder = inputBaseFolder;
    this.inputFileName = inputFileName;
  }


  private InputStream selectInputFile() throws FileNotFoundException {
    InputStream fileInputStream = null;

    if (inputBaseFolder != null) {
      fileInputStream = new FileInputStream(inputBaseFolder + File.separator + inputFileName);
    } else {
      fileInputStream = this.getClass().getClassLoader().getResourceAsStream(inputFileName);
      if (fileInputStream == null) {
        throw new FileNotFoundException();
      }
    }
    return fileInputStream;
  }

  private Map<Integer, RoadMapping> mapCities(String cityLine, int cityCount)
      throws WrongInputException {
    Map<Integer, RoadMapping> roadMappingSet = new HashMap<>();

    String[] split = cityLine.split(" ");
    if (split.length < 2 || split.length > 600 || (split.length != cityCount - 1)) {
      throw new WrongInputException("Wrong Input at roadmap, city line -> " + cityLine);
    }
    if (split.length != (cityCount - 1)) {
      logger.error("Conflict with city count and given city size");
    }

    for (int i = 0; i < split.length; i++) {
      int city1 = Integer.parseInt(split[i]);
      if (city1 < 0) {
        throw new WrongInputException(
            "City Index cannot be lower than zero, city line -> " + cityLine);
      }
      int city2 = i + 1;

      if (city1 > cityCount - 1 || city2 > cityCount - 1) {
        throw new WrongInputException(
            "City Index out of the range cities ,city line -> " + cityLine);
      }

      if (city1 != city2) {
        RoadMapping roadMapping1 = roadMappingSet.get(city1);
        RoadMapping roadMapping2 = roadMappingSet.get(city2);

        if (roadMapping1 == null) {
          roadMapping1 = new RoadMapping(city1);
          roadMappingSet.put(city1, roadMapping1);
        }

        if (roadMapping2 == null) {
          roadMapping2 = new RoadMapping(city2);
          roadMappingSet.put(city2, roadMapping2);
        }

        roadMapping1.getConnectedCities().add(city2);
        roadMapping2.getConnectedCities().add(city1);
        if (logger.isTraceEnabled()) {
          logger.trace("Added RoadMapping1 -> " + roadMapping1);
          logger.trace("Added RoadMapping2 -> " + roadMapping2);
        }

      } else {
        logger.error("Wrong Input -> " + cityLine);
      }
    }

    return roadMappingSet;
  }

  /**
   * Parse Input File To Input Descriptor Model To Use In Strategy
   * {@link com.sony.app.engine.StateUnifierStrategy}
   * 
   * @return Input Descriptor
   * @throws IOException
   * @throws WrongInputException
   */
  public InputDescriptor parse() throws IOException, WrongInputException {
    InputDescriptor inputDescriptor = new InputDescriptor();
    BufferedReader reader = null;
    InputStream fileInputStream = null;
    try {
      fileInputStream = selectInputFile();

      reader = new BufferedReader(new InputStreamReader(fileInputStream));
      String currentLine = reader.readLine();

      if (currentLine != null) {
        inputDescriptor.setTestCaseCount(Integer.parseInt(currentLine));
        if (logger.isTraceEnabled()) {
          logger.trace("Input Test Case Count -> " + inputDescriptor.getTestCaseCount());
        }
      }
      List<InputCase> inputCases = new ArrayList<>();
      while (currentLine != null) {
        InputCase inputCase = new InputCase();

        String cityCount = reader.readLine();
        if (cityCount != null) {
          int cityCountInt = Integer.parseInt(cityCount);
          inputCase.setCityCount(cityCountInt);
          String roadMap = reader.readLine();
          if (roadMap != null) {
            Map<Integer, RoadMapping> roadMapping = mapCities(roadMap, cityCountInt);
            inputCase.setRoadMappingMapping(roadMapping);
            inputCases.add(inputCase);
          } else {
            logger.error("Corrupted file");
            break;
          }
        } else {
          if (logger.isInfoEnabled()) {
            logger.info("File read process is completed");
          }
          break;
        }

      }
      inputDescriptor.setInputCases(inputCases);
      return inputDescriptor;
    } catch (IOException e) {
      logger.error("No Input File => " + inputBaseFolder + File.separator + inputFileName, e);
    } finally {
      if (fileInputStream != null) {
        try {
          fileInputStream.close();
        } catch (IOException e) {
          logger.error(
              "An Exception Occured While Closing The File Input Stream -> " + e.getMessage(), e);
        }
      }
      if (reader != null) {
        try {
          reader.close();
        } catch (IOException e) {
          logger.error("An Exception Occured While Closing The Input Reader  -> " + e.getMessage(),
              e);
        }
      }
    }
    return null;
  }
}
