package com.sony.app.input;

import com.sony.app.type.RoadMapping;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class InputCase {
  private int cityCount;
  private Map<Integer, RoadMapping> roadMappingMapping = new HashMap<>();

  public int getCityCount() {
    return cityCount;
  }

  public void setCityCount(int cityCount) {
    this.cityCount = cityCount;
  }

  public Map<Integer, RoadMapping> getRoadMappingMapping() {
    return roadMappingMapping;
  }

  public void setRoadMappingMapping(Map<Integer, RoadMapping> roadMappingMapping) {
    this.roadMappingMapping = roadMappingMapping;
  }

  @Override
  public String toString() {
    return "InputCase{" + "cityCount=" + cityCount + ", roadMappingMapping=" + roadMappingMapping
        + '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o)
      return true;
    if (o == null)
      return false;
    InputCase inputCase = (InputCase) o;
    return cityCount == inputCase.cityCount
        && Objects.equals(roadMappingMapping, inputCase.roadMappingMapping);
  }

  @Override
  public int hashCode() {

    return Objects.hash(cityCount, roadMappingMapping);
  }
}
