package com.sony.app.input;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class InputDescriptor {
  List<InputCase> inputCases = new ArrayList<>();
  private int testCaseCount;

  public int getTestCaseCount() {
    return testCaseCount;
  }

  public void setTestCaseCount(int testCaseCount) {
    this.testCaseCount = testCaseCount;
  }

  public List<InputCase> getInputCases() {
    return inputCases;
  }

  public void setInputCases(List<InputCase> inputCases) {
    this.inputCases = inputCases;
  }

  @Override
  public String toString() {
    return "InputDescriptor{" + "testCaseCount=" + testCaseCount + ", inputCases=" + inputCases
        + '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o)
      return true;
    if (o == null || getClass() != o.getClass())
      return false;
    InputDescriptor that = (InputDescriptor) o;
    return testCaseCount == that.testCaseCount && Objects.equals(inputCases, that.inputCases);
  }

  @Override
  public int hashCode() {

    return Objects.hash(testCaseCount, inputCases);
  }
}
